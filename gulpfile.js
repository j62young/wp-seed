const del = require('del');
const gulp = require('gulp');
const gulpCopy = require('gulp-copy');
const gulpSass = require('gulp-sass');
const rename = require('gulp-rename');
const notify = require('gulp-notify');
const gulpWatch = require('gulp-watch');
const zip = require('gulp-zip');
const path = require('path');

gulp.task('sass', function() {
  return gulp
    .src(['./style/main.scss'])
    .pipe(gulpSass()
      .on('', gulpSass.logError))
    .pipe(rename('style.css'))
    .pipe(gulp.dest('./'))
    .pipe(notify('Scss compiled!'));
});

gulp.task('build', function() {
  gulp
    .src(['./style/main.scss'])
    .pipe(gulpSass()
      .on('', gulpSass.logError))
    .pipe(rename('style.css'))
    .pipe(gulp.dest('./'))
    .pipe(gulpCopy('./seed-theme'))
    .pipe(notify('Sass build complete!'));
  return gulp
    .src(['./templates/**/*','./static/**/*', './*.php'], { base : './' })
    //insert path that you want to deploy theme to
    .pipe(gulpCopy('./seed-theme'))
    .pipe(notify({ message: "All tasks complete.", onLast: true }));
});

gulp.task('clean', function(){
  return del(['./style.css', './seed-theme', '../seed-theme.zip'], {force: true});
});

// Prod tasks
gulp.task('build-prod',  gulp.series('clean', 'build',function(){
    return gulp.src('./seed-theme/**/*', { base : './' })
            .pipe(zip('seed-theme.zip'))
            .pipe(gulp.dest('../'))
            .pipe(notify({ message: "Build complete.", onLast: true }));
}));

gulp.task('build-prod[watch]', function(){
   gulpWatch(['./style/**/*.scss', './templates/**/*.twig', './*.php'], gulp.series('build-prod'));
});
