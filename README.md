### Joshua C. Young
# wp-seed for Wordpress theme development

A Wordpress starter project.

## Installing the Theme

1. Add Timber as a plugin to your Wordpress instance.
2. Run yarn install.
3. Run yarn build or yarn build-watch. This script will create a seed-theme directory that can be put in a deployment environment.

## What's here?

`static/` is where you can keep your static front-end scripts, styles, or images. In other words, your Sass files, JS files, fonts, and SVGs would live here.

`templates/` contains all of your Twig templates. These pretty much correspond 1 to 1 with the PHP files that respond to the WordPress template hierarchy. At the end of each PHP template, you'll notice a `Timber::render()` function whose first parameter is the Twig file where that data (or `$context`) will be used. Just an FYI.

`bin/` and `tests/` ... basically don't worry about (or remove) these unless you know what they are and want to.
